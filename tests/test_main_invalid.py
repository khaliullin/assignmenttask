from starlette.testclient import TestClient

from main import app

client = TestClient(app)


def test_json_is_none():
    response = client.post(
        "/",
        json=None
    )
    assert response.status_code == 422
    assert "field required" in response.text


def test_json_is_invalid():
    response = client.post(
        "/",
        json="Bad"
    )
    assert response.status_code == 422
    assert "value is not a valid" in response.text
