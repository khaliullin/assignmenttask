from starlette.testclient import TestClient

from main import app

client = TestClient(app)


def test_json_is_object():
    response = client.post(
        "/",
        json={
            "glossary": {
                "title": "example glossary",
                "GlossDiv": {
                    "title": "S",
                    "GlossList": {
                        "GlossEntry": {
                            "ID": "SGML",
                            "SortAs": "SGML",
                            "GlossTerm": "Standard Generalized Markup Language",
                            "Acronym": "SGML",
                            "Abbrev": "ISO 8879:1986",
                            "GlossSee": "markup"
                        }
                    }
                }
            }
        }
    )
    assert response.status_code == 200
    assert response.json() == {"success": True}


def test_json_is_list():
    response = client.post(
        "/",
        json=[
            {
                "_id": "617b94554beb0c0064196819",
                "index": 0,
                "guid": "bad088da-c665-4dd5-8d66-6e0a4eda0c3b",
                "isActive": True,
                "balance": "$1,276.41",
                "picture": "http://placehold.it/32x32",
                "age": 27,
                "email": "westwest@automon.com",
                "phone": "+1 (800) 423-2968",
                "tags": [
                    "laborum",
                    "irure",
                    "id",
                    "consequat",
                    "anim",
                    "irure",
                    "quis"
                ]
            },
            {
                "_id": "617b94559b4697f15a4056eb",
                "friends": [
                    {
                        "id": 0,
                        "name": "Carrillo Joseph"
                    },
                    {
                        "id": 1,
                        "name": "Larsen Dennis"
                    },
                    {
                        "id": 2,
                        "name": "Sally Oneil"
                    }
                ],
                "favoriteFruit": "banana"
            }
        ]
    )
    assert response.status_code == 200
    assert response.json() == {"success": True}


def test_json_is_empty():
    response = client.post(
        "/",
        json={}
    )
    assert response.status_code == 200
    assert response.json() == {"success": True}
