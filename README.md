# Test assignment

### Create python code to accept API request from outside, payload is in JSON format. Response in JSON format. Iterate all JSON values in python code (print them)

Steps to run this project on local machine:
1. Install all requirements 
   
`pip install -r requirements.txt`

2. Run local web server 
   
`uvicorn main:app`

3. Send POST request to 127.0.0.1:8000/ with any JSON payload
