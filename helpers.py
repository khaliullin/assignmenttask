import numbers

from consts import JSONStructure


def print_json_values(json_obj: JSONStructure):
    """
    Recursively iterates through JSON-like object and prints its values.

    :param json_obj: JSON-like python object (list or dict)
    """
    if isinstance(json_obj, dict):
        for key, value in json_obj.items():
            print_json_values(value)
    elif isinstance(json_obj, list):
        for item in json_obj:
            print_json_values(item)
    elif isinstance(json_obj, (str, bool, numbers.Number)):
        print(json_obj)
