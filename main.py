from fastapi import FastAPI
from starlette.responses import JSONResponse

from consts import JSONStructure
from helpers import print_json_values


app = FastAPI()


@app.get("/")
async def index_get():
    return "Send POST query with JSON payload to this address."


@app.post("/")
async def index_post(json_data: JSONStructure):
    print_json_values(json_data)
    return JSONResponse(content={"success": True})
